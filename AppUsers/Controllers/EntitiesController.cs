﻿using AppUsers.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AppUsers.Controllers
{
    public class EntitiesController : ApiController
    {

        /// <summary>
        /// Add a new Entity.
        /// </summary>
		/// <param name="name"></param>
		/// <param name="email"></param>
		/// <param name="nib"></param>
		/// <param name="password"></param>
        /// <returns>Added Guest</returns>
        /// <returns>Token já existe</returns>
        /// <returns>Token Invalido</returns>
        public HttpResponseMessage get_NewEntity(string name, string email, string nif, string password)
        {
			Entity newentity = new Entity();
			newentity.name = name;
			newentity.nif = nif;
			newentity.password = password;
            newentity.date = DateTime.Now;

            // 1st check on email
            if (IsValidEmail(email) == true)
                newentity.email = email;

            if (this.ModelState.IsValid && newentity.email != null)
            {
                dbEntitiesModel.addnewentity(newentity);

                if (newentity.token != null)
                {
					var entity = new { id = newentity.id, nif = newentity.nif, token = newentity.token};
					var response = Request.CreateResponse(HttpStatusCode.Created, entity);
                    return response;
                }
            }
            return Request.CreateResponse("Invalid Entity...");
        }


		/// <summary>
		/// Add a new User by Entity Invite
		/// Send the Invite
		/// </summary>
		/// <param name="idEntity"></param>
		/// <param name="name"></param>
		/// <param name="email"></param>
		/// <returns>Added User</returns>
		/// <returns>Email já existe</returns>
		/// <returns>Email Invalido</returns>
		public HttpResponseMessage Get_NewUser_byEntity_Invite(int idEntity, string name, string email)
		{
			User newuser = new User();
			newuser.name = name;
			newuser.active = 0;
			newuser.date = DateTime.Now;

			// 1st check on email
			if (IsValidEmail(email) == true)
				newuser.email = email;

			if (this.ModelState.IsValid && newuser.email != null)
			{
				dbUserModel.addnewuser(newuser);

				if (newuser.token != null)
				{
					var response = Request.CreateResponse(HttpStatusCode.Created, "Novo Convite e Utilizador Criado - userID:" + newuser.id + " - Token:" + newuser.token);
					invitation(idEntity, email);
					return response;
				}
				else
					invitation(idEntity, email);
				return Request.CreateResponse("Novo convite enviado para  " + email);

			}
			return Request.CreateResponse(email + " - Email Invalido");
		}


        /// <summary>
        /// Update Guest Active Status.
        /// </summary>
        /// <param name="token"></param>
        /// <returns>JToken</returns>
        public IHttpActionResult get_UpdateActive(string token)
        {
            bool guest = dbGuestsModel.updateactive(token);
            return Ok((JToken)guest);
        }

		/// <summary>
		/// Get User Email by token.
		/// </summary>
		/// <param name="email"></param>
		/// <returns>The User with the specified Email</returns>
		public IHttpActionResult Get_login(string nif, string password)
		{
			Entity entity = dbEntitiesModel.searchEntity_forLogin(nif, password);
			if (entity == null)
			{
				return NotFound();
			}
			var logged = new { id = entity.id, nif = entity.nif, email = entity.email, token = entity.token, active = entity.active };
			return Ok(logged);
		}

		//
		// Private Functions
		//

		/// <summary>
		/// Send Entity Invite
		/// </summary>
		/// <param name="idEntity"></param>
		/// <param name="email"></param>
		/// <returns></returns>
		private bool invitation(int idEntity, string email)
		{
			try
			{
				dbInvitesModel.addnewinvite_entity(idEntity, email);
				return true;
			}
			catch
			{
				return false;
			}
		}


		/// <summary>
		/// Check System.Net.Mail for valid email
		/// </summary>
		/// <param name="email"></param>
		/// <returns>valid email</returns>
		/// <returns>false</returns>
		private bool IsValidEmail(string email)
		{
			try
			{
				var addr = new System.Net.Mail.MailAddress(email);
				return addr.Address == email;
			}
			catch
			{
				return false;
			}
		}

    }
}


















///// <summary>
//    /// Entity invite / Add a new Guest.
//    /// </summary>
//    /// <param name="Guest"></param>
//    /// <returns>Added Guest</returns>
//    /// <returns>Token já existe</returns>
//    /// <returns>Token Invalido</returns>
//    public HttpResponseMessage Get_newentity_Invite(int entity, string name, string email)
//    {
//        Guest newentity = new Guest();
//        newentity.active = 0;
//        newentity.date = DateTime.Now;

//        if (this.ModelState.IsValid && newentity.token == null)
//        {
//            dbGuestModel.addnewentity(newentity);

//            if (newentity.token != null)
//            {
//                var response = Request.CreateResponse<Guest>(HttpStatusCode.Created, newentity); // Resposta = 0bj newentity
//                return response;

//            }
//        }
//        return Request.CreateResponse("Token Invalido");
//    }


///// <summary>
///// Get Guest by Email.
///// </summary>
///// <param name="email"></param>
///// <returns>The User with the specified Email</returns>
//public IHttpActionResult GetEmail(string email, string token)
//{
//	Guest guest = dbGuestModel.getguest(email, token);

//	if (guest == null)
//	{
//		return NotFound();
//	}
//	return Ok(guest);
//}