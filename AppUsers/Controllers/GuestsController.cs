﻿using AppUsers.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Web.Http;

namespace AppUsers.Controllers
{
    public class GuestsController : ApiController
    {

        /// <summary>
        /// Add a new Guest.
        /// </summary>
		/// <param name="idEntity"></param>
        /// <returns>Added Guest</returns>
        /// <returns>Token já existe</returns>
        /// <returns>Token Invalido</returns>
        public IHttpActionResult get_NewGuest(int idEntity)
        {
            
            Guest newguest = new Guest();
            newguest.id_Ent = idEntity;
            newguest.date = DateTime.Now;
            newguest.active = 0;

            if (this.ModelState.IsValid && newguest.token == null)
            {
                dbGuestsModel.addnewguest(newguest);

                if (newguest.token != null)
                {
                    var guest = new { id = newguest.id, token = newguest.token, active = newguest.active };
                    return Ok(guest);
                }
            }
            return NotFound();
        }


        /// <summary>
        /// Update Guest Active Status.
        /// </summary>
        /// <param name="token"></param>
        /// <returns>JToken</returns>
        public IHttpActionResult get_UpdateActive(string token)
        {
            bool guest = dbGuestsModel.updateactive(token);
            return Ok((JToken)guest);
        }
    }
}