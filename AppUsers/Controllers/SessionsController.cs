﻿using AppUsers.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Net.Http;
using System.Web.Http;

namespace AppUsers.Controllers
{
	public class SessionsController : ApiController
	{

		/// <summary>
		/// Get Session by token & email
		/// </summary>
		/// <param name="token"></param>
		/// <param name="email"></param>
		/// <returns></returns>
		public HttpResponseMessage GetSession(string email, string token)
		{
			var check = dbSessionsModel.checktoken(token);

			if (check == null)
			{
				var ps = psession(email, token);

				if (ps != null)
				{
					var ns = dbSessionsModel.addnewsession(ps);
					var session = new { id = ns.id, token = ns.token, active = ns.active };

					return Request.CreateResponse(session);
				}
				else
					return Request.CreateResponse("Token inválido");
			}
			else
				//// Active status
				if (check != null)
				{
					var logged = check.active;
					return Request.CreateResponse(logged);
				}
				else
					return Request.CreateResponse(0);

		}

		/// <summary>
		/// Get Session by token & email
		/// </summary>
		/// <param name="nif"></param>
		/// <param name="token"></param>
		/// <returns></returns>
		public HttpResponseMessage GetEntitySession(string nif, string token)
		{
			var check = dbSessionsModel.checktoken(token);

			if (check == null)
			{
				var entity = pentity(nif, token);

				if (entity != null)
				{
					var ns = dbSessionsModel.addnewsession(entity);
					var session = new { id = ns.id, token = ns.token, active = ns.active };

					return Request.CreateResponse(session);
				}
				else
					return Request.CreateResponse("Token inválido");
			}
			else
				//// Active status
				if (check != null)
				{
					var logged = check.active;
					return Request.CreateResponse(logged);
				}
				else
					return Request.CreateResponse(0);

		}

		public IHttpActionResult get_UpdateActive(int id, int account, int check)
		{
			bool update = false;
			switch(check){
				case 1:
					update = dbGuestsModel.accountstatus(id, check);
					break;
				case 2:
					update = dbUserModel.accountstatus(id, check);
					break;
				case 3:
					update = dbEntitiesModel.accountstatus(id, check);
					break;
			}
			return Ok((JToken)update);
		}

		//
		// Private Session Function
		//

		/// <summary>
		/// Session for User or Guest
		/// </summary>
		/// <param name="token"></param>
		/// <param name="email"></param>
		/// <returns>Guest or User Session</returns>
		private Session psession(string email, string token)
		{
			if (email != "Guest")
			{
				User user = dbUserModel.searchUserID(token);
				if (user != null)
				{
					Session usession = new Session();
					usession.idUser = user.id;
					usession.token = token;
					usession.date = DateTime.Now;
					usession.active = 1;
					return usession;
				}
				return null;
			}
			else
			{
				Guest guest = dbGuestsModel.searchguest(token);
				if (guest != null)
				{
					Session gsession = new Session();
					gsession.idGuest = guest.id;
					gsession.token = token;
					gsession.date = DateTime.Now;
					gsession.active = 1;
					return gsession;
				}
				return null;
			}
		}

		private Session pentity(string nif, string token)
		{

			Entity entity = dbEntitiesModel.searchentity(token);
			if (entity != null)
			{
				Session esession = new Session();
				esession.idEntity = entity.id;
				esession.token = token;
				esession.date = DateTime.Now;
				esession.active = 1;
				return esession;
			}
			else return null;
		}

	}
}