﻿using System;
using System.Collections.Generic;
using System.Linq;
using AppUsers.Models;
using Newtonsoft.Json.Linq;
using System.Net;
using System.Web.Http;

namespace AppUsers.Controllers
{
	public class SlidesController : ApiController
	{

		/// <summary>
		/// Get a single Slide
		/// </summary>
		/// <param name="id">Slide id</param>
		/// <returns>Return Slide</returns>

		public IHttpActionResult GetSlide(int id)
		{

            Slide slide = dbSlideModel.getslide(id);

			if (slide == null)
			{
				return NotFound();
			}
			return Ok(slide);
		}

		/// <summary>
		/// Get all Slides in a Category
		/// </summary>
		/// <param name="category"></param>
		/// <returns>Return Category Slides</returns>

		public IQueryable<Slide> GetSlideByCategory(string category)
		{

            Slide[] slides = dbSlideModel.getslideCat(category).ToArray<Slide>();
			
			if (category == null)
			{
				// Response 200 ok Status but [] // do something?
			}
			return slides.AsQueryable().Where(
				p => string.Equals(p.category, category, StringComparison.OrdinalIgnoreCase));
		}

	}
}