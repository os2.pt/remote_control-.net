﻿using AppUsers.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AppUsers.Controllers
{

	public class UsersController : ApiController
	{

		/// <summary>
		/// Add a new User.
		/// </summary>
		/// <param name="User"></param>
		/// <returns>Added User</returns>
		/// <returns>Email já existe</returns>
		/// <returns>Email Invalido</returns>
		public HttpResponseMessage Get_NewUser(string name, string lastname, string email, string password)
		{
			User newuser = new User();
			newuser.name = name;
			newuser.lastname = lastname;
			newuser.password = password;
			newuser.date = DateTime.Now;
			newuser.active = 0;

			// 1st check on email
			if (IsValidEmail(email) == true)
				newuser.email = email;

			if (this.ModelState.IsValid && newuser.email != null)
			{
				dbUserModel.addnewuser(newuser);

				if (newuser.token != null)
				{
					var user = new { id = newuser.id, email = newuser.email, token = newuser.token };
					var response = Request.CreateResponse(HttpStatusCode.Created, user);
					return response;
				}
				else
					return Request.CreateResponse("Email já existe");

			}
			return Request.CreateResponse("Email Invalido");
		}


		/// <summary>
		/// Get User by Token.
		/// </summary>
		/// <param name="id">User ID</param>
		/// <returns>The User with the specified ID</returns>
		public IHttpActionResult get_UpdateActive(string token)
		{
			bool user = dbUserModel.upadateactive(token);
			return Ok((JToken)user);
		}


		/// <summary>
		/// Get User Email by token.
		/// </summary>
		/// <param name="email"></param>
		/// <returns>The User with the specified Email</returns>
		public IHttpActionResult Get_login(string email, string password)
		{
			User user = dbUserModel.searchUser_forLogin(email, password);
			if (user == null)
			{
				return NotFound();
			}
			var logged = new {id=user.id, email = user.email, token=user.token, active = user.active};
			return Ok(logged);
		}


		/// <summary>
		/// Add a new User by another User Invite.
		/// </summary>
		/// <param name="User"></param>
		/// <returns>Added User</returns>
		/// <returns>Email já existe</returns>
		/// <returns>Email Invalido</returns>
		public HttpResponseMessage Get_NewUser_inviteuser(int userid, string invitedemail)
		{
			User newuser = new User();
			newuser.active = 0;
			newuser.date = DateTime.Now;

			// 1st check on email
			if (IsValidEmail(invitedemail) == true)
				newuser.email = invitedemail;

			if (this.ModelState.IsValid && newuser.email != null)
			{
				dbUserModel.addnewuser(newuser);

				if (newuser.token != null)
				{
					var response = Request.CreateResponse<User>(HttpStatusCode.Created, newuser);
					invitationuser(userid, invitedemail);
					return response;
				}
				else
					invitationuser(userid, invitedemail);
				return Request.CreateResponse("Novo convite enviado para " + invitedemail);
			}
			return Request.CreateResponse(invitedemail + " - Email Invalido");
		}


		//
		// Private Functions
		//


		/// <summary>
		/// Check System.Net.Mail for valid email
		/// </summary>
		/// <param name="email"></param>
		/// <returns>valid email</returns>
		/// <returns>false</returns>
		private bool IsValidEmail(string email)
		{
			try
			{
				var addr = new System.Net.Mail.MailAddress(email);
				return addr.Address == email;
			}
			catch
			{
				return false;
			}
		}


		/// <summary>
		/// Send User Invite
		/// </summary>
		/// <param name="idEntity"></param>
		/// <param name="email"></param>
		/// <returns></returns>
		private bool invitationuser(int inviter, string invited)
		{
			try
			{
				dbInvitesModel.addnewinvite_user(inviter, invited);
				return true;
			}
			catch
			{
				return false;
			}
		}
	}
}