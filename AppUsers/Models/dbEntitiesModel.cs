﻿using Newtonsoft.Json.Linq;
using System;
using System.Configuration;
using System.Linq;

namespace AppUsers.Models
{
	public class dbEntitiesModel
	{

		/// <summary>
		/// Connection to DB User
		/// </summary>
		/// <returns>connection</returns>
		private static dbUserDataContext conn()
		{
			string connection = ConfigurationManager.ConnectionStrings["dbUserConnectionString"].ConnectionString;
			dbUserDataContext db = new dbUserDataContext(connection);

			return db;
		}


		/// <summary>
		/// Add New Guest
		/// If token don´t exists
		/// Get a newuser Token
		/// </summary>
        /// <param name="newentity"></param>
		/// <returns>JToken</returns>
		public static JToken addnewentity(Entity newentity)
		{
			var db = conn();

            // Check if Entity exists
            var user = from e in db.Entities
                       where e.nif == newentity.nif
                       select e.nif;

            var result = user.FirstOrDefault();

            if (result == null)
            {
                byte[] time = BitConverter.GetBytes(DateTime.Now.ToBinary());
                byte[] key = Guid.NewGuid().ToByteArray();
                string input = Convert.ToBase64String(time.Concat(key).ToArray());
                newentity.token = input.Replace("+", "B").Replace("/", "S");

                db.Entities.InsertOnSubmit(newentity);
                db.SubmitChanges();

                // Enviar Email aqui...
                return (JToken)true;
            }
            return (JToken)false;
        }


		/// <summary>
		/// Guest by Token
		/// </summary>
		/// <param name="token"></param>
		/// <returns>Guest</returns>
		public static bool updateactive(string token)
		{
			var db = conn();

			// Check token
			var query = from u in db.Entities
					   where u.token == token
					   select u;

            var result = query.FirstOrDefault();

			if (result != null && result.active == 0)
			{
				result.active = 1;
				db.SubmitChanges();
				return true;
			}
			return false;
		}


		/// <summary>
		/// Guest by Token
		/// </summary>
		/// <param name="token"></param>
		/// <returns>Entity</returns>
		public static Entity searchentity(string token)
		{
			var query = from e in conn().Entities
					   where e.token == token
					   select e;
            return query.SingleOrDefault<Entity>();
		}

		/// <summary>
		/// User by email and password
		/// </summary>
		/// <param name="id"></param>
		/// <returns>User</returns>
		public static Entity searchEntity_forLogin(string nif, string password)
		{
			var entity = from e in conn().Entities
					   where e.nif == nif && e.password == password
					   select e;

			return entity.SingleOrDefault<Entity>();
		}


		/// <summary>
		/// Generate Ramdom 6 Char String
		/// </summary>
		/// <returns>finalString</returns>
		public static string ramdom()
		{
			var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
			var stringChars = new char[6];
			var random = new Random();

			for (int i = 0; i < stringChars.Length; i++)
			{
				stringChars[i] = chars[random.Next(chars.Length)];
			}
			var finalString = new String(stringChars);

			return finalString;
		}


		public static bool accountstatus(int id, int check)
		{
			// INCOMPLETO
			var db = conn();

			// Check token
			var query = from u in db.Entities
						where u.id == id
						select u;

			var result = query.FirstOrDefault();

			if (result != null)
			{
				result.active = (short)((check == 1) ? 1 : 0);
				db.SubmitChanges();
				return true;
			}
			return false;
		}
	}
}