﻿using Newtonsoft.Json.Linq;
using System;
using System.Configuration;
using System.Linq;

namespace AppUsers.Models
{
	public class dbGuestsModel
	{

		/// <summary>
		/// Connection to DB User
		/// </summary>
		/// <returns>connection</returns>
		private static dbUserDataContext conn()
		{
			string connection = ConfigurationManager.ConnectionStrings["dbUserConnectionString"].ConnectionString;
			dbUserDataContext db = new dbUserDataContext(connection);

			return db;
		}


		/// <summary>
		/// Add New Guest
		/// If token don´t exists
		/// Get a newuser Token
		/// </summary>
        /// <param name="newguest"></param>
		/// <returns>JToken</returns>
		public static JToken addnewguest(Guest newguest)
		{
			var db = conn();
			string result = null;

			do
			{
				var ran = ramdom();
				newguest.token = ran;

				// Check if token exists
				var query = from u in db.Guests
							where u.token == newguest.token
							select u.token;
                result = query.FirstOrDefault();
			}
			while (result != null);
			db.Guests.InsertOnSubmit(newguest);
			db.SubmitChanges();
			return (JToken)true;
		}


		/// <summary>
		/// Guest by Token
		/// </summary>
		/// <param name="token"></param>
		/// <returns>Guest</returns>
		public static bool updateactive(string token)
		{
			var db = conn();

			// Check token
			var query = from u in db.Guests
					   where u.token == token
					   select u;

            var result = query.FirstOrDefault();

			if (result != null && result.active == 0)
			{
				result.active = 1;
				db.SubmitChanges();
				return true;
			}
			return false;
		}


		/// <summary>
		/// Guest by Token
		/// </summary>
		/// <param name="token"></param>
		/// <returns>Guest</returns>
		public static Guest searchguest(string token)
		{
			var query = from g in conn().Guests
					   where g.token == token
					   select g;
            return query.SingleOrDefault<Guest>();
		}


		/// <summary>
		/// Generate Ramdom 6 Char String
		/// </summary>
		/// <returns>finalString</returns>
		public static string ramdom()
		{
			var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
			var stringChars = new char[6];
			var random = new Random();

			for (int i = 0; i < stringChars.Length; i++)
			{
				stringChars[i] = chars[random.Next(chars.Length)];
			}
			var finalString = new String(stringChars);

			return finalString;
		}

		public static bool accountstatus(int id, int check)
		{
			var db = conn();

			// Check token
			var query = from u in db.Guests
						where u.id == id
						select u;

			var result = query.FirstOrDefault();

			if (result != null)
			{
				result.active = (short)((check==1)?1:0);
				db.SubmitChanges();
				return true;
			}
			return false;
		}
	}
}