﻿using Newtonsoft.Json.Linq;
using System;
using System.Configuration;
using System.Linq;

namespace AppUsers.Models
{
	public class dbInvitesModel
	{
		
		/// <summary>
		/// Connection to DB User
		/// </summary>
		/// <returns>connection</returns>
		private static dbUserDataContext conn()
		{
			string connection = ConfigurationManager.ConnectionStrings["dbUserConnectionString"].ConnectionString;
			dbUserDataContext db = new dbUserDataContext(connection);

			return db;
		}


		/// <summary>
		/// Add User Invite
		/// Check if User exists
		/// </summary>
		/// <param name="idEntity"></param>
		/// <param name="email"></param>
		/// <returns></returns>
		public static JToken addnewinvite_user(int iduser, string email)
		{
			var db = conn();

			// Check if invited user existes
			var query = from u in db.Users
						 where u.email == email
						select u;

			var result = query.FirstOrDefault();

			if (result != null)
			{
				Invite_User newinvite = new Invite_User();
				newinvite.idInviter = iduser;
				newinvite.idInvited = result.id;
				newinvite.invite = DateTime.Now;
				newinvite.token = ramdom();

				db.Invite_Users.InsertOnSubmit(newinvite);
				db.SubmitChanges();
				return (JToken)true;
			}
			else
 			return (JToken)false;

		}

		/// <summary>
		/// Add Entity Invite
		/// Check if email exists
		/// </summary>
		/// <param name="idEntity"></param>
		/// <param name="email"></param>
		/// <returns></returns>
		public static JToken addnewinvite_entity(int idEntity, string email)
		{
			var db = conn();

			// Check if invited user existes
			var query = from u in db.Users
						where u.email == email
						select u;

			var result = query.FirstOrDefault();

			if (result != null)
			{
				Invite_Entity newinvite = new Invite_Entity();
				newinvite.idEntities = idEntity;
				newinvite.idUsers = result.id;
				newinvite.invite = DateTime.Now;
				newinvite.token = dbGuestsModel.ramdom();

				db.Invite_Entities.InsertOnSubmit(newinvite);
				db.SubmitChanges();
				return (JToken)true;
			}
			else
				return (JToken)false;
		}


		/// <summary>
		/// Generate Ramdom 8 Char String
		/// </summary>
		/// <returns>finalString</returns>
		public static string ramdom()
		{
			var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
			var stringChars = new char[8];
			var random = new Random();

			for (int i = 0; i < stringChars.Length; i++)
			{
				stringChars[i] = chars[random.Next(chars.Length)];
			}
			var finalString = new String(stringChars);

			return finalString;
		}

	}
}