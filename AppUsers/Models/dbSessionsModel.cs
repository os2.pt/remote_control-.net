﻿using System;
using System.Configuration;
using System.Linq;

namespace AppUsers.Models
{
	public class dbSessionsModel
	{

		/// <summary>
		/// Connection to DB User
		/// </summary>
		/// <returns>connection</returns>
		public static dbUserDataContext conn()
		{
			string connection = ConfigurationManager.ConnectionStrings["dbUserConnectionString"].ConnectionString;
			dbUserDataContext db = new dbUserDataContext(connection);

			return db;
		}


		/// <summary>
		/// Add a new Session to DB
		/// </summary>
		/// <param name="newsession"></param>
		/// <returns></returns>
		public static Session addnewsession(Session newsession)
		{
			newsession.token = ramdom();
			var db = conn();
			db.Sessions.InsertOnSubmit(newsession);
			db.SubmitChanges();

			return newsession;
		}


		/// <summary>
		/// Check if Session token already exists
		/// </summary>
		/// <param name="token"></param>
		/// <returns></returns>
		public static Session checktoken(string token)
		{
			var query = from s in conn().Sessions
						where s.token == token
						select s;

			return query.SingleOrDefault<Session>();
		}


		/// <summary>
		/// Generate Ramdom 6 Char String
		/// </summary>
		/// <returns>finalString</returns>
		public static string ramdom()
		{
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
			var stringChars = new char[8];
			var random = new Random();

			for (int i = 0; i < stringChars.Length; i++)
			{
				stringChars[i] = chars[random.Next(chars.Length)];
			}
			var finalString = new String(stringChars);

			return finalString;
		}

	}
}