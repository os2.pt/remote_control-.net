﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace AppUsers.Models
{
	public class dbSlideModel
	{

        /// <summary>
        /// Connection to DB User
        /// </summary>
        /// <returns>connection</returns>
        private static dbUserDataContext conn()
        {
            string connection = ConfigurationManager.ConnectionStrings["dbUserConnectionString"].ConnectionString;
            dbUserDataContext db = new dbUserDataContext(connection);

            return db;
        }
		
		/// <summary>
		/// Get from DB a single Slide
		/// </summary>
		/// <param name="id">Slide id</param>
		/// <returns>Return Slide</returns>	
		public static Slide getslide(int id)
		{


            var slide = from s in conn().Slides
						where s.id == id
						select s;

			return slide.SingleOrDefault<Slide>();
		}


		/// <summary>
		/// Get from DB all Slides in a Category
		/// </summary>
		/// <param name="category">Category string</param>
		/// <returns>Return Category Slides</returns>

		public static List<Slide> getslideCat(string category)
		{


			List<Slide> dev = new List<Slide>();

            var slide = from s in conn().Slides
						where s.category == category
						select s;

			foreach (Slide s in slide)
			{
				dev.Add(s);
			}

			return dev;
		}

	}
}