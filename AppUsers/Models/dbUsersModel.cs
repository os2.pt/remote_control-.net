﻿using Newtonsoft.Json.Linq;
using System;
using System.Configuration;
using System.Linq;

namespace AppUsers.Models
{
	public class dbUserModel
	{
		/// <summary>
		/// Connection to DB User
		/// </summary>
		/// <returns>connection</returns>
		private static dbUserDataContext conn()
		{
			string connection = ConfigurationManager.ConnectionStrings["dbUserConnectionString"].ConnectionString;
			dbUserDataContext db = new dbUserDataContext(connection);

			return db;
		}


		/// <summary>
		/// Search User by ID
		/// </summary>
		/// <param name="id"></param>
		/// <returns>User</returns>
		public static User searchUser(int id)
		{
			var user = from u in conn().Users
					   where u.id == id
					   select u;

			return user.SingleOrDefault<User>();
		}


		/// <summary>
		/// Search User by ID
		/// </summary>
		/// <param name="token"></param>
		/// <returns>User</returns>
		public static User searchUserID(string token)
		{
			var user = from u in conn().Users
					   where u.token == token
					   select u;

			return user.SingleOrDefault<User>();
		}


		/// <summary>
		/// User by email and password
		/// </summary>
		/// <param name="id"></param>
		/// <returns>User</returns>
		public static User searchUser_forLogin(string email, string password)
		{
			var user = from u in conn().Users
					   where u.email == email && u.password == password
					   select u;
			
			return user.SingleOrDefault<User>();
		}


		/// <summary>
		/// User by email and password
		/// </summary>
		/// <param name="id"></param>
		/// <returns>User</returns>
		public static bool searchUser_forCookie(int id, string password)
		{
			var query = from u in conn().Users
						where u.id == id && u.password == password
						select u;

			var result = query.FirstOrDefault();

			if (result != null)
			{
				return true;
			}
			return false;
		}


		/// <summary>
		/// Add New User
		/// Check If email exists on DB
		/// Get a newuser Token
		/// </summary>
		/// <returns>JToken</returns>
		public static JToken addnewuser(User newuser)
		{
			var db = conn();

			// Check if user exists
			var user = from u in db.Users
					   where u.email == newuser.email
					   select u.email;

			var result = user.FirstOrDefault();

			if (result == null)
			{
				byte[] time = BitConverter.GetBytes(DateTime.Now.ToBinary());
				byte[] key = Guid.NewGuid().ToByteArray();
				string input = Convert.ToBase64String(time.Concat(key).ToArray());
				newuser.token = input.Replace("+", "").Replace("/", "");

				db.Users.InsertOnSubmit(newuser);
				db.SubmitChanges();

				// Enviar Email aqui...
				return (JToken)true;
			}
			return (JToken)false;
		}


		/// <summary>
		/// Update Active Status
		/// </summary>
		/// <param name="token"></param>
		/// <returns></returns>
		public static bool upadateactive(string token)
		{
			var db = conn();

			// Check token
			var query = from u in db.Users
						where u.token == token
						select u;

			var result = query.FirstOrDefault();

			if (result != null && result.active == 0)
			{
				result.active = 1;
				db.SubmitChanges();
				return true;
			}
			return false;
		}


		public static bool accountstatus(int id, int check)
		{
			// INCOMPLETO
			var db = conn();

			// Check token
			var query = from u in db.Users
						where u.id == id
						select u;

			var result = query.FirstOrDefault();

			if (result != null)
			{
				result.active = (short)((check==1)?1:0);
				db.SubmitChanges();
				return true;
			}
			return false;
		}
	}
}










///// <summary>
///// Get Users Log in Info
///// </summary>
///// <param name="email"></param>
///// <param name="password"></param>
///// <returns></returns>
//public static User searchlogin(string email, string password)
//{
//	var user = from u in conn().Users
//			   where u.email == email
//			   select u;

//	return user.SingleOrDefault<User>();
//}





///// <summary>
///// Get user by Token, and Update Active Status
///// </summary>
///// <param name="id"></param>
///// <param name="token"></param>
///// <returns>JToken True or False</returns>
//public static JToken updateactive(string token)
//{
//    var db = conn();

//    // Check email
//    var user = from u in db.Users
//               where u.token == token
//               select u;

//    var result = user.FirstOrDefault();

//    if (result != null)
//    {
//        result.active = 1;

//        db.SubmitChanges();
//        return (JToken)true;
//    }

//    return (JToken)false;

//}



///// <summary>
///// Get from DB all Users in a search
///// </summary>
///// <param name="category">Category string</param>
///// <returns>Return Category Slides</returns>

//public static List<User> getUserSearch(string category)
//{

//	string connection = ConfigurationManager.ConnectionStrings["dbUsersConnectionString"].ConnectionString;
//	dbUserDataContext db = new dbUserDataContext(connection);

//	List<Slide> dev = new List<Slide>();

//	var slide = from s in db.Slides
//				where s.category == category
//				select s;

//	foreach (Slide s in slide)
//	{
//		dev.Add(s);
//	}

//	return dev;
//}