﻿
// Vars

var uri = 'api/slides';
var index = 'index.html'
var thisPage = window.location.pathname.substring(1);

// language

var en = ['Title - ', 'Description - ', 'Price - $'];
var pt = ['Titulo - ', 'Descrição - ', 'Preço - $'];
var es = ['Título - ', 'Descripción - ', 'precio - $'];

// Check language

if (getUrlParameter('lang') != null) {

	lang = getUrlParameter('lang');

	if (lang == 'en') language = en;
	if (lang == 'pt') language = pt;
	if (lang == 'es') language = es;
}
else
    language = en;

//
// Url Parameter
//

function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
}

// Get url - category

if (getUrlParameter('category') == null) {

	//UnComment for debug
	cat = '01';
}
else {

	cat = getUrlParameter('category');
}

//
//Connection to Hub
//

var name = $.cookie('id');
var contosoChatHubProxy = $.connection.contosoChatHub;
contosoChatHubProxy.client.addContosoChatMessageToPage = function (name, message) {
    console.log(name + ' ' + message + ' ' + name);

	// Hub Messages

    if (message == "next" && $.cookie('id') == name) {
		$('.frame').sly('next');
	}

    if (message == "prev" && $.cookie('id') == name) {
		$('.frame').sly('prev');
	}

    if (message == "info" && $.cookie('id') == name) {

		getid = $('.active').attr("data-id");
		contosoChatHubProxy.server.newContosoChatMessage(name, getid);
	}

    if (message == "en" && $.cookie('id') == name) {

		if (language != en)
			window.location.href = thisPage + "?lang=en&category=" + cat;
	}

    if (message == "pt" && $.cookie('id') == name) {

		if (language != pt)
			window.location.href = thisPage + "?lang=pt&category=" + cat;
	}

    if (message == "es" && $.cookie('id') == name) {

		if (language != es)
			window.location.href = thisPage + "?lang=es&category=" + cat;
	}

    if (message == "home" && $.cookie('id') == name) {

	    //window.location = index;
	}

};
$.connection.hub.start().done()

$(document).ready(function () {

	//Call Web api to get items
	function loaditems() {
		// Send an AJAX request
		$.getJSON(uri + '?category=' + cat)

			.done(function (data) {
				// On success, 'data' contains a list of Slides.
				slide = data;
				//Design slideshow
				design();

			});

		// ---------------------------------------------------
		//   Load App - Centered Navigation - Sly_Ign
		// ---------------------------------------------------

		function design() {

			// load HTML
			var $slideWrapper = $('body');

			$slideHTML = '<div class="pagespan container"><div class="wrap"><div class="scrollbar"><div class="handle" style="transform: translateZ(0px) translateX(0px); width: 190px;"><div class="mousearea"></div></div></div>';
			$slideHTML += '<div class="frame" id="centered" style="overflow: hidden;"><ul class="clearfix" style="transform: translateZ(0px) translateX(0px); width: 6840px;">';

			// Load item data
			$.each(slide, function (index, item) {
				$slideHTML += '<li data-id="' + item.id + '">';
				$slideHTML += '<span id="l1"><b>' + language[0] + '</b></span><span class="s1">' + item.title + '<br></span>';
				$slideHTML += '<span id="l2"><b>' + language[1] + '</b></span><span class="s2">' + item.desc + '<br></span>';
				$slideHTML += '<span id="l3"><b>' + language[2] + '</b></span><span class="s3">' + item.price + '<br></span>';
				$slideHTML += '<img class="img" src="/img/' + item.category + '/' + item.url + '" />';
				$slideHTML += '</li>';
			});

			$slideHTML += '</ul></div><div class="controls center">';
			$slideHTML += '</div></div></div>';
			$slideWrapper.append($slideHTML);

			designSlideshow();
		}

		function designSlideshow() {

			var $frame = $('#centered');
			var $wrap = $frame.parent();

			// Call Sly on frame - load options
			$frame.sly({
				horizontal: 1,
				itemNav: 'centered',
				smart: 1,
				activateOn: 'click',
				mouseDragging: 1,
				touchDragging: 1,
				releaseSwing: 1,
				startAt: 4,
				scrollBar: $wrap.find('.scrollbar'),
				scrollBy: 1,
				speed: 300,
				elasticBounds: 1,
				easing: 'easeOutExpo',
				dragHandle: 1,
				dynamicHandle: 1,
				clickBar: 1,
				prev: $wrap.find('.prev'),
				next: $wrap.find('.next')
			});
		}
	}

	loaditems();
});

