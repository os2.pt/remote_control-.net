﻿
// Vars
var homePage = "thankyou.html";
var emailPage = "Info_sendEmail.html"

// Language 
var en = ['Prev', 'Next', 'More Info', 'Home']
var pt = ['Anterior', 'Seguinte', 'Mais Informações', 'Inicio']
var es = ['Anterior', 'Siguiente', 'Más información', 'Inicio']

// Load html

$(document).ready(function () {

    var $Wrapper = $('body');

	$html = '<input type="button" id="en" value="EN" class="langbtt" />';
	$html += '<input type="button" id="es" value="ES" class="langbtt" />';
	$html += '<input type="button" id="pt" value="PT" class="langbtt" />';
	$html += '<br><br><fieldset><b><legend><b>SlideShow Control</legend></b><hr>';
	$html += '<input type="button" id="prev" value="Prev" class="butt" />';
	$html += '<input type="button" id="next" value="Next" class="buttN" /><br /><br />';
	$html += '<input type="button" id="info" value="+ Info" class="butt2" /><br />';
	$html += '<input type="button" id="home" value="Home" class="butt2" />';
	$html += '</fieldset>';
	$Wrapper.append($html);


	if ($.cookie('lang') == null) language("en");
	else language($.cookie('lang'));

});

// Get Select and Change Language

function language(selected) {

	var sel = select(selected)
	$('#prev').val(sel[0]);
	$('#next').val(sel[1]);
	$('#info').val(sel[2]);
	$('#home').val(sel[3]);
}
function select(lang) {

	if (lang == 'pt') return pt;
	if (lang == 'es') return es;
	return en;
}

//
// Url Parameter
//

function getUrlParameter(sParam) {
	var sPageURL = window.location.search.substring(1);
	var sURLVariables = sPageURL.split('&');
	for (var i = 0; i < sURLVariables.length; i++) {
		var sParameterName = sURLVariables[i].split('=');
		if (sParameterName[0] == sParam) {
			return sParameterName[1];
		}
	}
}

//Get Selected item id

if (getUrlParameter('getid') == null) {
	var getid = 1;
}
else {
	var getid = getUrlParameter('getid');
}


//
// Hub Connection
//

var name = $.cookie('id');
var contosoChatHubProxy = $.connection.contosoChatHub;
contosoChatHubProxy.client.addContosoChatMessageToPage = function (name, message) {
	console.log(name + ' ' + message);

	if (message != 'next' && message != 'prev' && message != 'info' && message != 'home' && message != 'en' && message != 'pt' && message != 'es')
		window.location.href = emailPage + "?getid=" + message + "&lang=" + $.cookie("lang") + " ";
};
$.connection.hub.start().done(function () {

	// button clicks

	$('#prev').click(function () {
		contosoChatHubProxy.server.newContosoChatMessage(name, "prev");
	});
	$('#next').click(function () {
		contosoChatHubProxy.server.newContosoChatMessage(name, "next");
	});
	$('#info').click(function () {
		contosoChatHubProxy.server.newContosoChatMessage(name, "info");
	});
	$('#home').click(function () {
		contosoChatHubProxy.server.newContosoChatMessage(name, "home");
		window.location.href = homePage;
	});
	$('#en').click(function () {
		contosoChatHubProxy.server.newContosoChatMessage(name, "en");
		$.cookie("lang", "en", { expires: 30, path: "/" });
		language('en');
	});
	$('#pt').click(function () {
		contosoChatHubProxy.server.newContosoChatMessage(name, "pt");
		$.cookie("lang", "pt", { expires: 30, path: "/" });
		language('pt');
	});
	$('#es').click(function () {
		contosoChatHubProxy.server.newContosoChatMessage(name, "es");
		$.cookie("lang", "es", { expires: 30, path: "/" });
		language('es');
	});

});