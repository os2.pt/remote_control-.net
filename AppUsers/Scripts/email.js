﻿
// Vars
var uri = 'api/slides';

// languages

var en = ['Title - ', 'Description - ', 'Price - $', 'Send Email', 'Back'];
var pt = ['Título - ', 'Descrição - ', 'Preço - $', 'Envia Email', 'voltar'];
var es = ['Título - ', 'Descripción - ', 'precio - $', 'Envía Email', 'volver'];


// Load html

$(document).ready(function () {

    var $Wrapper = $('body');
    $html = '<fieldset><b><legend><b>SlideShow Control</legend></b><hr>';
	$html += '<p id="slideInfo"></p>';
	$html += '<h2>Name </h2><input type="text" id="name" class="box" />';
	$html += '<h2>Email </h2><input type="text" id="email" class="box" /><br /><br />';
	$html += '<input type="submit" id="send" value="Send Email" class="butt" />';
	$html += '<input type="button" id="back" value="back" class="butt" />';
	$Wrapper.append($html);

	//Call Web api to get items

	function formatItem(item) {
		var s = select(lang);
		$html = '<span id="title"><b>' + s[0] + ' </b></span><span class="s1">' + item.title + '<br></span>';
		$html += '<span id="desc"><b>' + s[1] + ' </b></span><span class="s2">' + item.desc + '<br></span>';
		$html += '<span id="price"><b>' + s[2] + ' </b><span class="s3">' + item.price + '<br></span></fieldset>';

		return $html
	}

	$.getJSON(uri + '/' + getid)
		.done(function (data) {
			obj = data;
			$("#slideInfo").html(formatItem(data));

		})
		.fail(function (jqXHR, textStatus, err) {
			$("#slideInfo").text('Error: ' + err);
			$('#product')
		});


	//Send email - Click

	$('#send').click(function () {

		//// Get vars for SMTP
		var name = $("#name").val();
		var email = $("#email").val();
		$.cookie("name", name);
		$.cookie("email", email);
		alert('Email enviado...')
		window.location.href = "control.html";

	});

	// back - click

	$('#back').click(function () {
		window.history.back()
	});

	// cookies - name, email and language

	if ($.cookie("name") != null && $("#email") != null) {

		$("#name").val($.cookie("name"));
		$("#email").val($.cookie("email"));
	}

	if ($.cookie("lang") != null) {

		language($.cookie("lang"));
	}

});

//
// Get Select and Change Language
//

if (getUrlParameter('lang') != null) {

	var lang = getUrlParameter('lang');

	if (lang == 'en') {

		language('en');
		$.cookie("lang", "en")
	}

	if (lang == 'pt') {

		language('pt');
		$.cookie("lang", "pt")
	}

	if (lang == 'es') {

		language('es');
		$.cookie("lang", "es")
	}

}
else {

	lang = en;
	$.cookie("lang", "en")
}

function language(selected) {

	var s = select(selected)
	$('#title').html(s[0]);
	$('#desc').text(s[1]);
	$('#price').append(s[2]);
	$('#send').val(s[3]);
	$('#back').val(s[4]);
}

function select(lang) {
	if (lang == 'pt') return pt;
	if (lang == 'es') return es;
	return en;
}

//
// Get Url Parameter
//

function getUrlParameter(sParam) {
	var sPageURL = window.location.search.substring(1);
	var sURLVariables = sPageURL.split('&');
	for (var i = 0; i < sURLVariables.length; i++) {
		var sParameterName = sURLVariables[i].split('=');
		if (sParameterName[0] == sParam) {
			return sParameterName[1];
		}
	}
}

//Get Selected item id

if (getUrlParameter('getid') == null) 
	getid = 1;
	
else 
	getid = getUrlParameter('getid');