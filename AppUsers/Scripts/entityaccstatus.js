﻿var session = 'api/Sessions';
var userlogin = "userlogin.html";
var homepage = 'homePage.html';
var index = 'index.html'

var en = ['Logged in as ', 'Index', 'HomePage', 'SignOut', 'Connected as '];
var pt = ['Conectado como ', 'Indice', 'Início', 'TerminarSessão', 'Sessão iniciada como '];
var es = ['Sessão iniciada como ', 'Índice', 'Inicio', 'Desconectar', 'Conectado como '];

$(document).ready(function () {

    var $Wrapper = $('body');
    $html = '<input type="button" id="en" value="EN" class="langbtt" />';
    $html += '<input type="button" id="es" value="ES" class="langbtt" />';
    $html += '<input type="button" id="pt" value="PT" class="langbtt" />';
    $html += '<input type="button" id="index" value="Index" class="btt" />';
    $html += '<input type="button" id="home" value="HomePage" class="btt" />';
    if ($.cookie('session') != null) {
        $html += '<input type="button" id="out" value="LogOut" class="btt" />';
        $html += '<div id="logoutbtt"><b><span id="con">Connected As </span></b>' + $.cookie('email') + '</div>';
    }
    $html += '<br><br><fieldset><b><legend id="title">' + $.cookie('email') + '</legend></b>';
    $html += '<p>Select Account ID</p><input type="textbox" name="id" id="id" class="box"><br>'
    $html += '<select class="select" id="sel">'
    $html += '<option value="1">Guests</option>'
    $html += '<option value="2">Users</option>'
    $html += '<option value="3">Entities</option>'
    $html += '</select><br><br>'
    $html += '<input type="checkbox" value="1" name="enable" id="c1" class="check">Enable Selected Account<br>'
    $html += '<input type="checkbox" value="2" name="disable" id="c2" class="check">Disable Selected Account<br>'
    $html += '<input type="checkbox" value="3" name="cancel" id="c3" class="check">Cancel Selected Account<br><br>'
    $html += '<input type="button" id="send" value="Submit" class="butt2" />';
    $Wrapper.append($html);

    // Check Session
    if ($.cookie("email") != null && $.cookie("session") != null) {
        $.getJSON(session + '?email=' + $.cookie("email") + '&token=' + $.cookie("session"))
			.done(function (data) {
			    if (data != null && data.active == 1)
			        window.location = homepage + "?email=" + $.cookie("email") + "&token=" + $.cookie("token");
			    else
			        if (data == 0)
			            window.location = userlogin;
			})
    }
    else
        window.location = userlogin;


    // CheckBox
    var check = null;
    $('#c1').on('change', function () {
        $('#c2').not(this).prop('checked', false);
        $('#c3').not(this).prop('checked', false);
        check = $(this);
    });
    $('#c2').on('change', function () {
        $('#c1').not(this).prop('checked', false);
        $('#c3').not(this).prop('checked', false);
        check = $(this);
    });
    $('#c3').on('change', function () {
        $('#c1').not(this).prop('checked', false);
        $('#c2').not(this).prop('checked', false);
        check = $(this);
    });


    // btt Clicks
    $('#send').click(function () {
        var id = $("#id").val();
        var account = $("#sel").find(":selected").val();
        if (check.is(":checked")) check = check.attr('value')
        else check = null;
        // API Session Logic
        if (id != null && account != null && check != null) {
            $.getJSON(session + '?id=' + id + '&account=' + account + '&check=' + check)
				.done(function (data) {
				    if (data == true)
				        alert("DB updated");

				});
        }
        else
            console.log("Selecte all options");
    });
    $('#out').click(function () {
        $.removeCookie('session', { path: '/' });
        window.location = index;
    });
    $('#index').click(function () {
        window.location = index;
    });
    $('#home').click(function () {
        window.location = homepage;
    });
    $('#en').click(function () {
        if ($.cookie('lang') != "en") {
            $.cookie("lang", "en", { expires: 30, path: "/" });
            language('en');
            window.location = window.location.pathname + '?lang=' + $.cookie('lang') + '&email=' + email + '&token=' + token;
        }
    });
    $('#pt').click(function () {
        if ($.cookie('lang') != "pt") {
            $.cookie("lang", "pt", { expires: 30, path: "/" });
            language('pt');
            window.location = window.location.pathname + '?lang=' + $.cookie('lang') + '&email=' + email + '&token=' + token;
        }
    });
    $('#es').click(function () {
        if ($.cookie('lang') != "es") {
            $.cookie("lang", "es", { expires: 30, path: "/" });
            language('es');
            window.location = window.location.pathname + '?lang=' + $.cookie('lang') + '&email=' + email + '&token=' + token;
        }
    });

    //Language
    if (getUrlParameter('lang') == null) {
        if ($.cookie('lang') == null) language("en");
        else language($.cookie('lang'));
    }
    else {
        language(getUrlParameter('lang'));
        $.cookie('lang', getUrlParameter('lang'));
    }

});

//
// Select and Change Language
//
function language(selected) {
    var sel = select(selected);
    $('#title').prepend(sel[0]);
    $('#index').val(sel[1]);
    $('#home').val(sel[2]);
    $('#out').val(sel[3]);
    $('#con').text(sel[4]);
}
function select(lang) {
    if (lang == 'pt') return pt;
    if (lang == 'es') return es;
    return en;
}

//
// Get Url Parameter
//

function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
}


//Get token

if (getUrlParameter('token') == null)
    var token = null;

else
    token = getUrlParameter('token');

//Get email

if (getUrlParameter('email') == null)
    var email = "Guest";

else
    email = getUrlParameter('email');


