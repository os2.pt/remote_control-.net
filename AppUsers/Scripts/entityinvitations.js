﻿var uri = 'api/Entities';
var homepage = 'homePage.html';
var index = 'index.html'

var en = ['Entity Invitation', 'Entity ID', 'Name', 'Email', 'Submit', 'Index', 'HomePage', 'SignOut', 'Connected as ' ];
var pt = ['Convite entidade', 'ID Entidade', 'Nome', 'E-Mail', 'Submeter', 'Indice', 'Início', 'TerminarSessão', 'Sessão iniciada como '];
var es = ['Invitación Entidad', 'Entidad ID', 'Nombre', 'Email', 'Someter', 'Índice', 'Inicio', 'Desconectar', 'Conectado como '];

$(document).ready(function () {

	var $Wrapper = $('body');
	$html = '<input type="button" id="en" value="EN" class="langbtt" />';
	$html += '<input type="button" id="es" value="ES" class="langbtt" />';
	$html += '<input type="button" id="pt" value="PT" class="langbtt" />';
	$html += '<input type="button" id="index" value="Index" class="btt" />';
	$html += '<input type="button" id="home" value="HomePage" class="btt" />';
	if ($.cookie('session') != null) {
		$html += '<input type="button" id="out" value="LogOut" class="btt" />';
		$html += '<div id="logoutbtt"><b><span id="con">Connected As </span></b>' + $.cookie('email') + '</div>';
	}
	$html += '<br><br><fieldset><b><legend id="title">Entity Invitation</legend></b><br>';
	$html += '<h2 id="lent">Entity ID</h2><input type="text" id="entity" class="box" />';
	$html += '<h2 id="lname">Name</h2><input type="text" id="name" class="box" />';
	$html += '<h2 id="lemail">Email</h2><input type="email" id="email" class="box" /><br /><br />';
	$html += '<input type="submit" id="send" class="butt2" value="Submit"></fieldset>';
	$Wrapper.append($html);

	//Get ID
	if ($.cookie('id') == null) { var id = null; }
	else { id = $.cookie('id'); $('#entity').val(id); }

	$('#send').click(function () {

		var idEntity = $("#entity").val();
		var name = $("#name").val();
		var email = $("#email").val();

		$.getJSON(uri + '?idEntity=' + idEntity + '&name=' + name + '&email=' + email)
			.done(function (data) {
				console.log(data);
			})
			.fail(function (jqXHR, textStatus, err) {
				console.log('Error: ' + err);
			});
	});
	// btt Clicks
	$('#out').click(function () {
		$.removeCookie('session', { path: '/' });
		window.location = index;
	});
	$('#index').click(function () {
	    window.location = index;
	});
	$('#home').click(function () {
	    window.location = homepage;
	});
	$('#en').click(function () {if ($.cookie('lang') != "en") {$.cookie("lang", "en", { expires: 30, path: "/" });
			language('en');
			window.location = window.location.pathname + '?lang=' + $.cookie('lang');
		}
	});
	$('#pt').click(function () {
		if ($.cookie('lang') != "pt") {$.cookie("lang", "pt", { expires: 30, path: "/" });
			language('pt');
			window.location = window.location.pathname + '?lang=' + $.cookie('lang');
		}
	});
	$('#es').click(function () {if ($.cookie('lang') != "es") {$.cookie("lang", "es", { expires: 30, path: "/" });
			language('es');
			window.location = window.location.pathname + '?lang=' + $.cookie('lang');
		}
	});

	// Language
	if (getUrlParameter('lang') == null) {
		if ($.cookie('lang') == null) language("en");
		else language($.cookie('lang'));
	}
	else {
		language(getUrlParameter('lang'));
		$.cookie('lang', getUrlParameter('lang'));
	}
});

//
// Select and Change Language
//

function language(selected) {
	var sel = select(selected);
	$('#title').text(sel[0]);
	$('#lent').text(sel[1]);
	$('#lname').text(sel[2]);
	$('#lemail').text(sel[3]);
	$('#send').val(sel[4]);
	$('#index').val(sel[5]);
	$('#home').val(sel[6]);
	$('#out').val(sel[7]);
	$('#con').text(sel[8]);
}
function select(lang) {
	if (lang == 'pt') return pt;
	if (lang == 'es') return es;
	return en;
}

//
// Get Url Parameter
//

function getUrlParameter(sParam) {
	var sPageURL = window.location.search.substring(1);
	var sURLVariables = sPageURL.split('&');
	for (var i = 0; i < sURLVariables.length; i++) {
		var sParameterName = sURLVariables[i].split('=');
		if (sParameterName[0] == sParam) {
			return sParameterName[1];
		}
	}
}