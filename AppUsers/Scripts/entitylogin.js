﻿var entity = 'api/Entities';
var session = 'api/Sessions';
var register = 'entityregistration.html';
var homepage = 'homePage.html';
var index = 'index.html'


var en = ['Entity Log In', 'NIF', 'Password', 'Submit', 'Log In as Guest', 'Need an Account?', 'Index', 'HomePage', 'SignOut', 'Connected as ' ];
var pt = ['Log In de Entidade', 'NIF', 'Password', 'Submeter', 'Entrar como convidado', 'Precisa de uma conta?', 'Indice', 'Início', 'TerminarSessão', 'Sessão iniciada como '];
var es = ['Ingreso de Entidad', 'NIF', 'Contraseña', 'Someter', 'Iniciar sesión como Invitado', 'Necesitas una cuenta?', 'Índice', 'Inicio', 'Desconectar', 'Conectado como '];

$(document).ready(function () {

	// html
	$wrapper = $('body');
	$html = '<input type="button" id="en" value="EN" class="langbtt" />';
	$html += '<input type="button" id="es" value="ES" class="langbtt" />';
	$html += '<input type="button" id="pt" value="PT" class="langbtt" />';
	$html += '<input type="button" id="index" value="Index" class="btt" />';
	$html += '<input type="button" id="home" value="HomePage" class="btt" />';
	if ($.cookie('session') != null) {
		$html += '<input type="button" id="out" value="LogOut" class="btt" />';
		$html += '<div id="logoutbtt"><b><span id="con">Connected As </span></b>' + $.cookie('email') + '</div>';
	}
	$html += '<br><br><fieldset><b><legend id="title">Entity Log In</legend><b><br />';
	$html += '<h2 id="lnib">NIF</h2>';
	$html += '<input type="text" id="boxnif" class="box" />';
	$html += '<h2 id="lpass">Password </h2>';
	$html += '<input type="password" id="pass" class="box" /><br /><br />';
	$html += '<input type="submit" id="login" class="butt2" value="Submit"><br />';
	$html += '<input type="submit" id="account" class="butt2" value="Need an Account?"></fieldset>';
	$wrapper.append($html);

	// Check Session
	if ($.cookie("nif") != null && $.cookie("session") != null) {
		$.getJSON(session + '?nif=' + $.cookie("nif") + '&token=' + $.cookie("session"))
			.done(function (data) {
				if (data == 1)
					window.location = homepage + "?nif=" + $.cookie("nif") + "&token=" + $.cookie("session");
			})
	}

	// Check Session and Gen Session Token
	function cookit(nif, token) {
		$.getJSON(session + '?nif=' + nif + '&token=' + token)
		.done(function (data) {
			if (data != null && data.active == 1) {
				$.cookie("session", data.token, { expires: 30, path: "/" });
				window.location = homepage + "?nif=" + nif + "&token=" + data.token;
			}
		})
	};

	//Get NIF
	if (getUrlParameter('nif') == null) { var nif = null; }
	else { nif = getUrlParameter('nif'); $('#boxnif').val(email); }
	if ($.cookie('nif') == null) { var nif = null; }
	else { nif = $.cookie('nif'); $('#boxnif').val(nif); }


	// btt Clicks
	$('#out').click(function () {
		$.removeCookie('session', { path: '/' });
		window.location = index;
	});
	$('#login').click(function () {
		var password = $("#pass").val();
		var nif = $("#boxnif").val();
	    // Log in
		if (nif != null && password != "") {
		    $.getJSON(entity + '?nif=' + nif + '&password=' + password)
                .done(function (data) {

                    if (data != null) {
                        $.cookie("id", data.id, { expires: 30, path: "/" });
                        $.cookie("nif", data.nif, { expires: 30, path: "/" });
                        $.cookie("email", data.email, { expires: 30, path: "/" });
                        $.cookie("token", data.token, { expires: 30, path: "/" });
                        cookit(data.nif, data.token);
                        $.cookie("session", cookit.token, { expires: 30, path: "/" });
                    }
                })
                .fail(function (jqXHR, textStatus, err) {
                    console.log('Error: ' + err);
                });
		}
	});
	$('#guest').click(function () {
		window.location = guest + '?lang=' + $.cookie('lang');
	});
	$('#account').click(function () {
		window.location = register + '?lang=' + $.cookie('lang');
	});

	// Set up Language Clicks
	// Links
	$('#index').click(function () {
		window.location = index;
	});
	$('#home').click(function () {
		window.location = homepage;
	});
	$('#en').click(function () {
		if ($.cookie('lang') != "en") {
			$.cookie("lang", "en", { expires: 30, path: "/" });
			language('en');
			window.location = window.location.pathname + '?lang=' + $.cookie('lang');
		}
	});
	$('#pt').click(function () {
		if ($.cookie('lang') != "pt") {
			$.cookie("lang", "pt", { expires: 30, path: "/" });
			language('pt');
			window.location = window.location.pathname + '?lang=' + $.cookie('lang');
		}
	});
	$('#es').click(function () {
		if ($.cookie('lang') != "es") {
			$.cookie("lang", "es", { expires: 30, path: "/" });
			language('es');
			window.location = window.location.pathname + '?lang=' + $.cookie('lang');
		}
	});

	// Get Selected Language
	if (getUrlParameter('lang') == null) {
		if ($.cookie('lang') == null) language("en");
		else language($.cookie('lang'));
	}
	else {
		language(getUrlParameter('lang'));
		$.cookie('lang', getUrlParameter('lang'));
	}

});

//
// Get Select and Change Language
//

function language(selected) {
	var sel = select(selected);
	$('#title').text(sel[0]);
	$('#lnib').text(sel[1]);
	$('#lpass').text(sel[2]);
	$('#login').val(sel[3]);
	$('#guest').val(sel[4]);
	$('#account').val(sel[5]);
	$('#index').val(sel[6]);
	$('#home').val(sel[7]);
	$('#out').val(sel[8]);
	$('#con').text(sel[9]);
}
function select(lang) {
	if (lang == 'pt') return pt;
	if (lang == 'es') return es;
	return en;
}

//
// Get Url Parameter
//

function getUrlParameter(sParam) {
	var sPageURL = window.location.search.substring(1);
	var sURLVariables = sPageURL.split('&');
	for (var i = 0; i < sURLVariables.length; i++) {
		var sParameterName = sURLVariables[i].split('=');
		if (sParameterName[0] == sParam) {
			return sParameterName[1];
		}
	}
}