﻿var entities = 'api/Entities';
var homepage = 'homePage.html';
var index = 'index.html'

var en = ['Entity Registration', 'Name', 'NIF', 'Email', 'Password', 'Submit', 'Index', 'HomePage', 'SignOut', 'Connected as '];
var pt = ['Registo de Entidades', 'Nome', 'NIF', 'E-Mail', 'Password', 'Submeter', 'Indice', 'Início', 'TerminarSessão', 'Sessão iniciada como '];
var es = ['Registro de Entidad', 'Nombre', 'NIF', 'Email', 'Contraseña', 'Someter', 'Índice', 'Inicio', 'Desconectar', 'Conectado como ' ];

$(document).ready(function () {

	var $Wrapper = $('body');
	$html = '<input type="button" id="en" value="EN" class="langbtt" />';
	$html += '<input type="button" id="es" value="ES" class="langbtt" />';
	$html += '<input type="button" id="pt" value="PT" class="langbtt" />';
	$html += '<input type="button" id="index" value="Index" class="btt" />';
	$html += '<input type="button" id="home" value="HomePage" class="btt" />';
	if ($.cookie('session') != null) {
		$html += '<input type="button" id="out" value="LogOut" class="btt" />';
		$html += '<div id="logoutbtt"><b><span id="con">Connected As </span></b>' + $.cookie('email') + '</div>';
	}
	$html += '<br><br><fieldset><b><legend id="title">User Registration</legend></b><br>';
	$html += '<h2 id="lname">Name </h2>';
	$html += '<input type="text" id="name" class="box" />';
	$html += '<h2 id ="lnif">NIF</h2>';
	$html += '<input type="text" id="nif" class="box" />';
	$html += '<h2 id="lemail">Email</h2>';
	$html += '<input type="email" id="email" class="box" />';
	$html += '<h2 id="lpass">Password</h2>';
	$html += '<input type="password" id="password" class="box" /><br /><br />';
	$html += '<input type="submit" id="send" class="butt2" value="Submit"></fieldset>';
	$Wrapper.append($html);

	$('#send').click(function () {
		var name = $("#name").val();
		var nif = $("#nif").val();
		var email = $("#email").val();
		var password = $("#password").val();

		$.getJSON(entities + '?name=' + name + '&nif=' + nif + '&email=' + email + '&password=' + password)
			.done(function (data) {
				if (data.id != null)
				    window.location = "entityactivation.html?email=" + data.email + "&token=" + data.token;
			})
			.fail(function (jqXHR, textStatus, err) {
				console.log('Error: ' + err);
			});
	});

	// btt Clicks
	$('#out').click(function () {
		$.removeCookie('session', { path: '/' });
		window.location = index;
	});
	$('#index').click(function () {
	    window.location = index;
	});
	$('#home').click(function () {
	    window.location = homepage;
	});
	$('#en').click(function () {
		if ($.cookie('lang') != "en") {$.cookie("lang", "en", { expires: 30, path: "/" });
			language('en');
			window.location = window.location.pathname + '?lang=' + $.cookie('lang');
		}
	});
	$('#pt').click(function () {
		if ($.cookie('lang') != "pt") {$.cookie("lang", "pt", { expires: 30, path: "/" });
			language('pt');
			window.location = window.location.pathname + '?lang=' + $.cookie('lang');
		}
	});
	$('#es').click(function () {if ($.cookie('lang') != "es") {$.cookie("lang", "es", { expires: 30, path: "/" });
			language('es');
			window.location = window.location.pathname + '?lang=' + $.cookie('lang');
		}
	});

	// Language
	if (getUrlParameter('lang') == null) {
		if ($.cookie('lang') == null) language("en");
		else language($.cookie('lang'));
	}
	else {
		language(getUrlParameter('lang'));
		$.cookie('lang', getUrlParameter('lang'));
	}
});

//
// Get Select and Change Language
//
function language(selected) {
	var sel = select(selected);
	$('#title').text(sel[0]);
	$('#lname').text(sel[1]);
	$('#lnib').text(sel[2]);
	$('#lemail').text(sel[3]);
	$('#lpass').text(sel[4]);
	$('#send').val(sel[5]);
	$('#index').val(sel[6]);
	$('#home').val(sel[7]);
	$('#out').val(sel[8]);
	$('#con').text(sel[9]);
}
function select(lang) {
	if (lang == 'pt') return pt;
	if (lang == 'es') return es;
	return en;
}

//
// Get Url Parameter
//

function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
}