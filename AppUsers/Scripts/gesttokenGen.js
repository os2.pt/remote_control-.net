﻿// Vars
var uri = 'api/guests';
var session = 'api/Sessions';
var homepage = 'homePage.html';
var index = 'index.html'
var url = window.location.href;

var en = ['Submit', 'Index', 'HomePage', 'SignOut', 'Connected as '];
var pt = ['Submeter', 'Indice', 'Início', 'TerminarSessão', 'Sessão iniciada como '];
var es = ['Someter', 'Índice', 'Inicio', 'Desconectar', 'Conectado como '];

$(document).ready(function () {

	// html Designer
	var $Wrapper = $('body');
	$html = '<input type="button" id="en" value="EN" class="langbtt" />';
	$html += '<input type="button" id="es" value="ES" class="langbtt" />';
	$html += '<input type="button" id="pt" value="PT" class="langbtt" />';
	$html += '<input type="button" id="index" value="Index" class="btt" />';
	$html += '<input type="button" id="home" value="HomePage" class="btt" />';
	if ($.cookie('session') != null) {
		$html += '<input type="button" id="out" value="LogOut" class="btt" />';
		$html += '<div id="logoutbtt"><b><span id="con">Connected As </span></b>' + $.cookie('email') + '</div>';
	}
	$html += '<br><br><fieldset><b><legend id="title">TokenGen</legend></b>';
	$html += '<p>idEntity - ' + idEntity + '</p>';
	$html += '<p id="ptoken">Token - </p>';
	$html += '<input type="submit" id="activ" class="butt2" value="Submit"></fieldset>';
	$Wrapper.append($html);

	// Gen 6 Char Guest Token
	$.getJSON(uri + '?idEntity=' + idEntity)
    .done(function (data) {
        $.cookie('token', data.token);
    	token = $.cookie('token');
    	$('#ptoken').append(data.token);
    })

	// btt Clicks
	$('#out').click(function () {
		$.removeCookie('session', { path: '/' });
		window.location = index;
	});
	$('#activ').click(function () {

		// Activate token
		$.getJSON(uri + '?token=' + token)
		.done(function (data) {
			if (data == true) {
				if ($.cookie('session') == null)
					cookit(token);
			}
		})

	});
	$('#index').click(function () {
		window.location = index;
	});
	$('#home').click(function () {
		window.location = homepage;
	});
	$('#en').click(function () {
		if ($.cookie('lang') != "en") {
			$.cookie("lang", "en", { expires: 30, path: "/" });
			language('en');
			window.location = window.location.pathname + '?lang=' + $.cookie('lang');
		}
	});
	$('#pt').click(function () {
		if ($.cookie('lang') != "pt") {
			$.cookie("lang", "pt", { expires: 30, path: "/" });
			language('pt');
			window.location = window.location.pathname + '?lang=' + $.cookie('lang');
		}
	});
	$('#es').click(function () {
		if ($.cookie('lang') != "es") {
			$.cookie("lang", "es", { expires: 30, path: "/" });
			language('es');
			window.location = window.location.pathname + '?lang=' + $.cookie('lang');
		}
	});

	// Set up Language
	if (getUrlParameter('lang') == null) {
		if ($.cookie('lang') == null) language("en");
		else language($.cookie('lang'));
	}
	else {
		language(getUrlParameter('lang'));
		$.cookie('lang', getUrlParameter('lang'));
	}

});

//
// Select and Change Language function
//

function language(selected) {
	var sel = select(selected);
	$('#activ').val(sel[0]);
	$('#index').val(sel[1]);
	$('#home').val(sel[2]);
	$('#out').val(sel[3]);
	$('#con').text(sel[4]);
}
function select(lang) {
	if (lang == 'pt') return pt;
	if (lang == 'es') return es;
	return en;
}


// Check Session and Gen Session Token
function cookit(token) {
	$.getJSON(session + '?email=Guest&token=' + token)
	.done(function (data) {
	    if (data != null && data.active == 1) {
	        $.cookie("id", data.id, { expires: 30, path: "/" });
			$.cookie("session", data.token, { expires: 30, path: "/" });
			$.cookie("email", "Guest", { expires: 30, path: "/" });
			window.location = homepage + "?email=Guest&token=" + token;
		}
	})
};

//
// Get Url Parameter
//

function getUrlParameter(sParam) {
	var sPageURL = window.location.search.substring(1);
	var sURLVariables = sPageURL.split('&');
	for (var i = 0; i < sURLVariables.length; i++) {
		var sParameterName = sURLVariables[i].split('=');
		if (sParameterName[0] == sParam) {
			return sParameterName[1];
		}
	}
}

//Get Entity

if (getUrlParameter('idEntity') == null)
	idEntity = 1;
else
	idEntity = getUrlParameter('idEntity');

//Get Token

if (getUrlParameter('token') == null)
	token = "ElTokenIsNotSet"
else
	token = getUrlParameter('token');

































//$('#activ').click(function () {

//	// Call web api

//	$.getJSON(uri + '?token=' + token)
//	.done(function (data) {


//	})
//	.fail(function (jqXHR, textStatus, err) {
//	});

//});