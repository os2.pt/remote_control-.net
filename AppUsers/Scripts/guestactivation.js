﻿// Vars
var uri = 'api/guests';
var email = "Guest";
var homepage = 'homePage.html';
var index = 'index.html'
var url = window.location.href;

var en = ['Insert Token to activate Guest account', 'Submit', 'Activation', 'Index', 'HomePage', 'SignOut', 'Connected as '];
var pt = ['Insira token para ativar conta de convidado', 'Submeter', 'Ativação', 'Indice', 'Início', 'TerminarSessão', 'Sessão iniciada como '];
var es = ['Inserte Token para activar la cuenta de invitado', 'Someter', 'Activación', 'Índice', 'Inicio', 'Desconectar', 'Conectado como '];

$(document).ready(function () {

    // html Designer

	var $Wrapper = $('body');
	$html = '<input type="button" id="en" value="EN" class="langbtt" />';
	$html += '<input type="button" id="es" value="ES" class="langbtt" />';
	$html += '<input type="button" id="pt" value="PT" class="langbtt" />';
	$html += '<input type="button" id="index" value="Index" class="btt" />';
	$html += '<input type="button" id="home" value="HomePage" class="btt" />';
	if ($.cookie('session') != null) {
		$html += '<input type="button" id="out" value="LogOut" class="btt" />';
		$html += '<div id="logoutbtt"><b><span id="con">Connected As </span></b>' + $.cookie('email') + '</div>';
	}
	$html += '<br><br><fieldset><b><legend id="title">Activation</legend></b>';
	$html += '<p id="info">Insert Token to activate Guest account<br></p><input type="text" class="box" id="ElToken"><br>';
	$html += '<input type="submit" id="activ" class="butt2" value="Submit"><br></fieldset>';
	$Wrapper.append($html);

	// btt Clicks
	$('#out').click(function () {
		$.removeCookie('session', { path: '/' });
		window.location = index;
	});
	$('#activ').click(function () {
		// Call web api
		$.getJSON(uri + '?token=' + $('#ElToken').val())
		.done(function (data) {
		    
		    if (data == true) {
		        alert(token + "- Foi activado");
				window.location = "HomePage.html?email=" + email + "&token=" + $('#ElToken').val();
			}
			else
				alert(token + "- Não é Valido");
		})
		.fail(function (jqXHR, textStatus, err) {
		});
	});
	$('#index').click(function () {
	    window.location = index;
	});
	$('#home').click(function () {
	    window.location = homepage;
	});
	$('#en').click(function () {if ($.cookie('lang') != "en") {$.cookie("lang", "en", { expires: 30, path: "/" });
			language('en');
			window.location = window.location.pathname + '?lang=' + $.cookie('lang');
		}
	});
	$('#pt').click(function () {if ($.cookie('lang') != "pt") {$.cookie("lang", "pt", { expires: 30, path: "/" });
			language('pt');
			window.location = window.location.pathname + '?lang=' + $.cookie('lang');
		}
	});
	$('#es').click(function () {if ($.cookie('lang') != "es") {$.cookie("lang", "es", { expires: 30, path: "/" });
			language('es');
			window.location = window.location.pathname + '?lang=' + $.cookie('lang');
		}
	});

	// Language
	if (getUrlParameter('lang') == null) {
		if ($.cookie('lang') == null) language("en");
		else language($.cookie('lang'));
	}
	else {
		language(getUrlParameter('lang'));
		$.cookie('lang', getUrlParameter('lang'));
	}

});

//
// Get Select and Change Language
//

function language(selected) {
	var sel = select(selected);
	$('#info').text(sel[0]);
	$('#activ').val(sel[1]);
	$('#title').text(sel[2]);
	$('#index').val(sel[3]);
	$('#home').val(sel[4]);
	$('#out').val(sel[5]);
	$('#con').text(sel[6]);
}
function select(lang) {
	if (lang == 'pt') return pt;
	if (lang == 'es') return es;
	return en;
}

//
// Get Url Parameter
//

function getUrlParameter(sParam) {
	var sPageURL = window.location.search.substring(1);
	var sURLVariables = sPageURL.split('&');
	for (var i = 0; i < sURLVariables.length; i++) {
		var sParameterName = sURLVariables[i].split('=');
		if (sParameterName[0] == sParam) {
			return sParameterName[1];
		}
	}
}

//Get token

if (getUrlParameter('token') == null)
	token = "Token";

else
	token = getUrlParameter('token');

//Get Entity

if (getUrlParameter('idEntity') == null)
	idEntity = 1;

else
	idEntity = getUrlParameter('idEntity');