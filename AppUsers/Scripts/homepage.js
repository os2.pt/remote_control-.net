﻿var session = 'api/Sessions';
var userlogin = "userlogin.html";
var homepage = 'homePage.html';
var index = 'index.html'
var slideshow = 'slideshow.html';
var control = 'control.html';

var en = ['Email: ', 'Index', 'HomePage', 'SignOut', 'ID: ', 'Token: ', 'Session Token: ', 'Selected Language: ', 'Connected as ', ];
var pt = ['E-Mail ', 'Indice', 'Início', 'TerminarSessão', 'ID: ', 'Token: ', 'Token de Sessão: ', 'Idioma Seleccionado: ', 'Sessão iniciada como '];
var es = ['Email: ', 'Índice', 'Inicio', 'Desconectar', 'ID', 'Token', 'Token de Sesión: ', 'Idioma Seleccionado: ', 'Conectado como '];

$(document).ready(function () {

    var $Wrapper = $('body');
    $html = '<input type="button" id="en" value="EN" class="langbtt" />';
    $html += '<input type="button" id="es" value="ES" class="langbtt" />';
    $html += '<input type="button" id="pt" value="PT" class="langbtt" />';
    $html += '<input type="button" id="index" value="Index" class="btt" />';
    $html += '<input type="button" id="home" value="HomePage" class="btt" />';
    if ($.cookie('session') != null) {
        $html += '<input type="button" id="out" value="LogOut" class="btt" />';
        $html += '<div id="logoutbtt"><b><span id="con">Connected As </span></b>' + $.cookie('email') + '</div>';
    }
    $html += '<br><br><fieldset><b><legend id="title">' + $.cookie('email') + '</legend></b><hr>';
    $html += '<input type="button" id="Slideshow" value="Slideshow" class="buttSS" />';
    $html += '<input type="button" id="control" value="Control App" class="buttSS" /><br><br>';
    $html += '<div id="qrcode"></div></fieldset>';
    $html += '<fieldset><b><legend id="title">Account Details - ' + $.cookie('email') + '</legend></b><hr>';
    $html += '<span id="lid"><b>ID: </b></span><span>' + $.cookie('id') + '</span><br><br>';
    $html += '<span id="ltoken"><b>Token: </b></span><span>' + $.cookie('token') + '</span><br><br>';
    $html += '<span id="stoken"><b>Session Token: </b></span><span>' + $.cookie('session') + '</span><br><br>';
    $html += '<span id="llang"><b>Selected Language: </b></span><span>' + $.cookie('lang') + '</span></fieldset>';
    $Wrapper.append($html);

    // Div it
    jQuery(function () {
        jQuery('#qrcode').qrcode(control + '?token=' + $.cookie("session"));
    });


    if ($.cookie("email") != null && $.cookie("session") != null) {
        $.getJSON(session + '?email=' + $.cookie("email") + '&token=' + $.cookie("session"))
			.done(function (data) {
			    if (data != null && data.active == 1)
			        window.location = homepage + "?email=" + $.cookie("email") + "&token=" + $.cookie("token");
			    else
			        if (data == 0)
			            window.location = userlogin;
			})
    }
    else
        window.location = userlogin;

    // btt Clicks
    $('#out').click(function () {
        $.removeCookie('session', { path: '/' });
        window.location = index;
    });
    $('#index').click(function () {
        window.location = index;
    });
    $('#home').click(function () {
        window.location = homepage;
    });
    $('#Slideshow').click(function () {
        window.open(slideshow + '?token=' + $.cookie("session"), '_blank');
    });
    $('#control').click(function () {
        window.open(control + '?token=' + $.cookie("session"), '_blank');
    });
    $('#en').click(function () {
        if ($.cookie('lang') != "en") {
            $.cookie("lang", "en", { expires: 30, path: "/" });
            language('en');
            window.location = window.location.pathname + '?lang=' + $.cookie('lang') + '&email=' + email + '&token=' + token;
        }
    });
    $('#pt').click(function () {
        if ($.cookie('lang') != "pt") {
            $.cookie("lang", "pt", { expires: 30, path: "/" });
            language('pt');
            window.location = window.location.pathname + '?lang=' + $.cookie('lang') + '&email=' + email + '&token=' + token;
        }
    });
    $('#es').click(function () {
        if ($.cookie('lang') != "es") {
            $.cookie("lang", "es", { expires: 30, path: "/" });
            language('es');
            window.location = window.location.pathname + '?lang=' + $.cookie('lang') + '&email=' + email + '&token=' + token;
        }
    });

    if (getUrlParameter('lang') == null) {
        if ($.cookie('lang') == null) language("en");
        else language($.cookie('lang'));
    }
    else {
        language(getUrlParameter('lang'));
        $.cookie('lang', getUrlParameter('lang'));
    }

});

//
// Select and Change Language
//
function language(selected) {
    var sel = select(selected);
    $('#title').prepend(sel[0]);
    $('#index').val(sel[1]);
    $('#home').val(sel[2]);
    $('#out').val(sel[3]);
    $('#lid').val(sel[4]);
    $('#ltoken').val(sel[6]);
    $('#llang').text(sel[7]);
    $('#con').text(sel[8]);

}
function select(lang) {
    if (lang == 'pt') return pt;
    if (lang == 'es') return es;
    return en;
}

//
// Get Url Parameter
//

function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
}


//Get token

if (getUrlParameter('token') == null)
    var token = null;
else
    token = getUrlParameter('token');

//Get email

if (getUrlParameter('email') == null)
    var email = "Guest";
else
    email = getUrlParameter('email');


