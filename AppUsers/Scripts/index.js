﻿var uri = "api/guests";
var qrcode = "qrcode.html";
var guesttokenGen = "guesttokenGen.html";
var guestactivation = "guestactivation.html";
var useractivation = "useractivation.html";
var userregistration = "userregistration.html";
var userinvitation = "userinvitations.html";
var userlogin = "userlogin.html";
var entityinvitation = "entityinvitations.html";
var entitylogin = "entitylogin.html";
var entityregistration = "entityregistration.html";
var entityactivation = "entityactivation.html";
var accstatus = "entityaccstatus.html";
var homepage = 'homePage.html';
var index = 'index.html'

var en = ['Guest TokenGen', 'Activation Link', 'Activation qrCode', 'Registration', 'Activation Link', 'User Invite', 'Log In', 'Registration', 'Activation Link', 'Invitations Entities', 'Log In', 'Index', 'HomePage', 'SignOut', 'Connected as ', 'Account Status' ];
var pt = ['TokenGen Visitante', 'Link de Activação', 'Activação qrCode', 'Registo', 'Link de Activação', 'Convite de Utilizador', 'Log In', 'Registo', 'Link de Activação', 'Convite de Entidade', 'Log In', 'Indice', 'Início', 'TerminarSessão', 'Sessão iniciada como ','Estado da Conta' ];
var es = ['TokenGen Invitado ', 'Activación Enlace', 'Activación qrCode', 'Registro', 'Activación Enlace', 'Invitar Usuario', 'Iniciar Sesión', 'Registro', 'Activación Enlace', 'Invitaciones Entidades', 'Iniciar Sesión', 'Índice', 'Inicio', 'Desconectar', 'Conectado como ', 'Estado de Cuenta'];

$(document).ready(function () {

	var $wrapper = $('body');
	$html = '<input type="button" id="en" value="EN" class="langbtt" />';
	$html += '<input type="button" id="es" value="ES" class="langbtt" />';
	$html += '<input type="button" id="pt" value="PT" class="langbtt" />';
	$html += '<input type="button" id="index" value="Index" class="btt" />';
	$html += '<input type="button" id="home" value="HomePage" class="btt" />';
	if ($.cookie('session') != null) {
		$html += '<input type="button" id="out" value="LogOut" class="btt" />';
		$html += '<div id="logoutbtt"><b><span id="con">Connected As </span></b>' + $.cookie('email') + '</div>';
	}
	$html += '<br><br><fieldset><b><legend><b>Entity</legend></b><hr><input type="submit" id="EntReg" class="butt2" value="Registration"><input type="submit" id="acte" class="butt2" value="Activation Link"><input type="submit" id="inve" class="butt2" value="Entity Invite"><br /><input type="submit" id="entitylogin" class="butt2" value="Log In"><br><hr><input type="submit" id="status" class="butt2" value="Account Status"></fieldset>';
	$html += '<fieldset><b><legend><b>User</legend></b><hr><input type="submit" id="reg" class="butt2" value="Registration"><br /><input type="submit" id="act" class="butt2" value="Activation Link"><br /><input type="submit" id="invu" class="butt2" value="User Invite"><br /><input type="submit" id="login" class="butt2" value="Log In"><br /></fieldset>';
	$html += '<fieldset><b><legend>Guest</legend></b><hr><input type="submit" id="guest" class="butt2" value="Guest TokenGen"><br /><input type="submit" id="actg" class="butt2" value="Activation Link"><br /><input type="submit" id="qrc" class="butt2" value="Activation qrCode"><br /></fieldset>';
	$wrapper.append($html);

	
	// btt Clicks
	$('#out').click(function () {
		$.removeCookie('session', { path: '/' });
		window.location = index;
	});
	$('#status').click(function () {
		window.location = accstatus;
	});
	$('#index').click(function () {
	    window.location = index;
	});
	$('#home').click(function () {
	    window.location = homepage;
	});
	$('#guest').click(function () {
	    window.location = guesttokenGen + '?idEntity=' + idEntity;
	});
	$('#reg').click(function () {
		window.location = userregistration;
	});
	$('#EntReg').click(function () {
		window.location = entityregistration;
	});
	$('#inve').click(function () {
		window.location = entityinvitation;
	});
	$('#invu').click(function () {
		window.location = userinvitation;
	});
	$('#qrc').click(function () {
		window.location = qrcode;
	});
	$('#act').click(function () {
		window.location = useractivation;
	});
	$('#actg').click(function () {
		window.location = guestactivation;
	});
	$('#acte').click(function () {
		window.location = entityactivation;
	});
	$('#login').click(function () {
		window.location = userlogin;
	});
	$('#entitylogin').click(function () {
		window.location = entitylogin;
	});

	//Language
	$('#en').click(function () {
		if ($.cookie('lang') != "en") {
			$.cookie("lang", "en", { expires: 30, path: "/" });
			language('en');
			window.location = window.location.pathname + '?lang=' + $.cookie('lang');
		}
	});
	$('#pt').click(function () {
		if ($.cookie('lang') != "pt") {
			$.cookie("lang", "pt", { expires: 30, path: "/" });
			language('pt');
			window.location = window.location.pathname + '?lang=' + $.cookie('lang');
		}
	});
	$('#es').click(function () {
		if ($.cookie('lang') != "es") {
			$.cookie("lang", "es", { expires: 30, path: "/" });
			language('es');
			window.location = window.location.pathname + '?lang=' + $.cookie('lang');
		}
	});

	if (getUrlParameter('lang') == null) {
		if ($.cookie('lang') == null) language("en");
		else language($.cookie('lang'));
	}
	else {
		language(getUrlParameter('lang'));
		$.cookie('lang', getUrlParameter('lang'));
	}

});

// Get Select and Change Language

if (getUrlParameter('lang') == null)
	lang = "en";

else
{
	lang = getUrlParameter('lang');
	language(lang);
}

function language(selected) {
	var sel = select(selected);
	$('#guest').val(sel[0]);
	$('#actg').val(sel[1]);
	$('#qrc').val(sel[2]);
	$('#reg').val(sel[3]);
	$('#act').val(sel[4]);
	$('#invu').val(sel[5]);
	$('#login').val(sel[6]);
	$('#EntReg').val(sel[7]);
	$('#acte').val(sel[8]);
	$('#inve').val(sel[9]);
	$('#entitylogin').val(sel[10]);
	$('#index').val(sel[11]);
	$('#home').val(sel[12]);
	$('#out').val(sel[13]);
	$('#con').text(sel[14]);
	$('#status').val(sel[15]);
}
function select(lang) {
	if (lang == 'pt') return pt;
	if (lang == 'es') return es;
	return en;
}

//
// Get Url Parameter
//

function getUrlParameter(sParam) {
	var sPageURL = window.location.search.substring(1);
	var sURLVariables = sPageURL.split('&');
	for (var i = 0; i < sURLVariables.length; i++) {
		var sParameterName = sURLVariables[i].split('=');
		if (sParameterName[0] == sParam) {
			return sParameterName[1];
		}
	}
}

//Get Entity

if (getUrlParameter('idEntity') == null)
	idEntity = 1;

else
	idEntity = getUrlParameter('idEntity');