﻿// Vars
var uri = 'api/guests';
var homepage = 'homePage.html';
var index = 'index.html'
var url = window.location.href;

$(document).ready(function () {

	// html Designer

	var $Wrapper = $('body');
	$html = '<div id="qrcode"></div><br>';
	$Wrapper.append($html);

	// Div it
	jQuery(function () {
		jQuery('#qrcode').qrcode(url);
	});

	// Call web api
	if (token != "ElTokenIsNotSet") {
		$.getJSON(uri + '?token=' + token)
		.done(function (data) {

			if (data == true){
				alert(token + "- Foi activado");
				window.location = homepage + "?email=Guest&token=" + token;
			}
			else
				alert(token + "- Não é Valido");
		})
	}

});

//
// Get Url Parameter
//

function getUrlParameter(sParam) {
	var sPageURL = window.location.search.substring(1);
	var sURLVariables = sPageURL.split('&');
	for (var i = 0; i < sURLVariables.length; i++) {
		var sParameterName = sURLVariables[i].split('=');
		if (sParameterName[0] == sParam) {
			return sParameterName[1];
		}
	}
}

//Get token

if (getUrlParameter('token') == null)
	token = "ElTokenIsNotSet";
else
	token = getUrlParameter('token');

//Get email

if (getUrlParameter('email') == null)
	email = "Guest";
else
	email = getUrlParameter('email');