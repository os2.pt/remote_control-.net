﻿var user = 'api/Users';
var session = 'api/Sessions';
var guest = 'guesttokenGen.html';
var register = 'userregistration.html';
var homePage = 'homePage.html';
var index = 'index.html'

var en = ['User Log In', 'Email', 'Password', 'Submit', 'Log In as Guest', 'Need an Account?', 'Index', 'HomePage', 'SignOut', 'Connected as ' ];
var pt = ['Log In de Utilizador', 'E-Mail', 'Password', 'Submeter', 'Entrar como convidado', 'Precisa de uma conta?', 'Indice', 'Início', 'TerminarSessão', 'Sessão iniciada como '];
var es = ['Ingreso de Usuario', 'Email', 'Contraseña', 'Someter', 'Iniciar sesión como Invitado', 'Necesitas una cuenta?', 'Índice', 'Inicio', 'Desconectar', 'Conectado como '];

$(document).ready(function () {

	// html
	$wrapper = $('body');
	$html = '<input type="button" id="en" value="EN" class="langbtt" />';
	$html += '<input type="button" id="es" value="ES" class="langbtt" />';
	$html += '<input type="button" id="pt" value="PT" class="langbtt" />';
	$html += '<input type="button" id="index" value="Index" class="btt" />';
	$html += '<input type="button" id="home" value="HomePage" class="btt" />';
	if ($.cookie('session') != null) {
		$html += '<input type="button" id="out" value="LogOut" class="btt" />';
		$html += '<div id="logoutbtt"><b><span id="con">Connected As </span></b>' + $.cookie('email') + '</div>';
	}
	$html += '<br><br><fieldset><b><legend id="title">User Log In</legend><b><br />';
	$html += '<h2 id="lemail">Email </h2>';
	$html += '<input type="text" id="email" class="box" />';
	$html += '<h2 id="lpass">Password </h2>';
	$html += '<input type="password" id="pass" class="box" /><br /><br>';
	$html += '<input type="submit" id="login" class="butt2" value="Submit"><br /><br>';
	$html += '<input type="submit" id="guest" class="butt2" value="Log In as Guest"><br />';
	$html += '<input type="submit" id="account" class="butt2" value="Need an Account?"></fieldset>';
	$wrapper.append($html);

	// Check Session
	if ($.cookie("email") != null && $.cookie("session") != null) {
		$.getJSON(session + '?email=' + $.cookie("email") + '&token=' + $.cookie("session"))
			.done(function (data) {
				if (data == 1)
					window.location = homePage + "?email=" + $.cookie("email") + "&token=" + $.cookie("session");
			})
	}

	// Check Session and Gen Session Token
	function cookit(email, token) {
		$.getJSON(session + '?email=' + email + '&token=' + token)
		.done(function (data) {
		    if (data != null && data.active == 1) {
		        $.cookie("session", data.token, { expires: 30, path: "/" });
		        window.location = homePage + "?email=" + email + "&token=" + data.token;
		    }
		    else
		        alert("User Not Found");
		})
	};

	//Get email
	if (getUrlParameter('email') == null) { var email = null; }
	else { email = getUrlParameter('email'); $('#email').val(email); }
	if ($.cookie('email') == null) { var email = null; }
	else { email = $.cookie('email'); $('#email').val(email); }


	// btt Clicks
	$('#out').click(function () {
		$.removeCookie('session', { path: '/' });
		window.location = index;
	});
	$('#login').click(function () {
	    var password = $("#pass").val();
	    var email = $("#email").val();
	    // Log in
        if(email != null && password != ""){
	    $.getJSON(user + '?email=' + email + '&password=' + password)
			.done(function (data) {
			    // Validade User
			    if (data != null) {
			        $.cookie("id", data.id, { expires: 30, path: "/" });
			        $.cookie("email", data.email, { expires: 30, path: "/" });
			        $.cookie("token", data.token, { expires: 30, path: "/" });
			        cookit(data.email, data.token);
			        $.cookie("session", cookit.token, { expires: 30, path: "/" });
			    }
			})
			.fail(function (jqXHR, textStatus, err) {
			    console.log('Error: ' + err);
			});
	}
	});

	$('#guest').click(function () {
		window.location = guest + '?lang=' + $.cookie('lang');
	});
	$('#account').click(function () {
		window.location = register + '?lang=' + $.cookie('lang');
	});
	$('#index').click(function () {
		window.location = index;
	});
	$('#home').click(function () {
		window.location = homepage;
	});
	// Set up Language Clicks
	$('#en').click(function () {
		if ($.cookie('lang') != "en") {
			$.cookie("lang", "en", { expires: 30, path: "/" });
			language('en');
			window.location = window.location.pathname + '?lang=' + $.cookie('lang');
		}
	});
	$('#pt').click(function () {
		if ($.cookie('lang') != "pt") {
			$.cookie("lang", "pt", { expires: 30, path: "/" });
			language('pt');
			window.location = window.location.pathname + '?lang=' + $.cookie('lang');
		}
	});
	$('#es').click(function () {
		if ($.cookie('lang') != "es") {
			$.cookie("lang", "es", { expires: 30, path: "/" });
			language('es');
			window.location = window.location.pathname + '?lang=' + $.cookie('lang');
		}
	});

	// Get Selected Language
	if (getUrlParameter('lang') == null) {
		if ($.cookie('lang') == null) language("en");
		else language($.cookie('lang'));
	}
	else {
		language(getUrlParameter('lang'));
		$.cookie('lang', getUrlParameter('lang'));
	}

});

//
// Select and Change Language
//

function language(selected) {
	var sel = select(selected);
	$('#title').text(sel[0]);
	$('#lemail').text(sel[1]);
	$('#lpass').text(sel[2]);
	$('#login').val(sel[3]);
	$('#guest').val(sel[4]);
	$('#account').val(sel[5]);
	$('#index').val(sel[6]);
	$('#home').val(sel[7]);
	$('#out').val(sel[8]);
	$('#con').text(sel[9]);
}
function select(lang) {
	if (lang == 'pt') return pt;
	if (lang == 'es') return es;
	return en;
}

//
// Get Url Parameter
//

function getUrlParameter(sParam) {
	var sPageURL = window.location.search.substring(1);
	var sURLVariables = sPageURL.split('&');
	for (var i = 0; i < sURLVariables.length; i++) {
		var sParameterName = sURLVariables[i].split('=');
		if (sParameterName[0] == sParam) {
			return sParameterName[1];
		}
	}
}

