﻿$(document).ready(function () {

	var currentPosition = 0;
	var slideWidth = 450;
	var slides = $('.slide');
	var numberOfSlides = slides.length;

	$wrapper = $('body');
	$html = '<div id="pageContainer"><div id="slideshow"><div id="slidesContainer"><div class="slide">'
	$html += '<img src="img/1.jpg" alt="Imagem 1" width="310" height="200" />'
	$html += '</div><div class="slide"><img src="img/2.jpg" alt="Imagem 2" width="310" height="200" />'
	$html += '</div><div class="slide"><img src="img/3.jpg" alt="Imagem 3" width="310" height="200" />'
	$html += '</div><div class="slide"><img src="img/4.jpg" alt="Imagem 4" width="310" height="200" />'
	$html += '</div><div class="slide"><img src="img/5.jpg" alt="Imagem 5" width="310" height="200" />'
	$html += '</div></div></div></div>'
	$wrapper.append($html);


	$('#slidesContainer').css('overflow', 'hidden');

	// Wrap .slides in #slideInner div
	slides
	  .wrapAll('<div id="slideInner"></div>')
	  .css({
	  	'float': 'left',
	  	'width': slideWidth
	  });

	// Set width equal to total width of all images
	$('#slideInner').css('width', slideWidth * numberOfSlides);

	// Insert Controls
	$('#slideshow')
	  .prepend('<span class="control" id="leftControl">Clicking moves left</span>')
	  .append('<span class="control" id="rightControl">Clicking moves right</span>');

	// Hide left arrow on first load
	manageControls(currentPosition);

	// Controls clicks
	$('.control')
	  .bind('click', function () {
	  	// Get new position
	  	currentPosition = ($(this).attr('id') == 'rightControl') ? currentPosition + 1 : currentPosition - 1;

	  	// Controls
	  	manageControls(currentPosition);
	  	$('#slideInner').animate({
	  		'marginLeft': slideWidth * (-currentPosition)
	  	});
	  });

	// Hides and Shows controls depending on currentPosition
	function manageControls(position) {
		if (position == 0) { $('#leftControl').hide() } else { $('#leftControl').show() }
		if (position == numberOfSlides - 1) { $('#rightControl').hide() } else { $('#rightControl').show() }
	}
});