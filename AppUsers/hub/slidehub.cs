﻿using Microsoft.AspNet.SignalR;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AppUsers.hub
{
    public class ContosoChatHub : Hub
    {
        public void NewContosoChatMessage(int id, string message)
        {
            Clients.All.addContosoChatMessageToPage(id, message);
        }
    }
}